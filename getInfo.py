# -*- coding: UTF-8 -*-
import sys
import os 
import time 
import psutil
import pynvml

#Get GPU info
def getGPUInfo():
    try:
        pynvml.nvmlInit()
        # 這裡的0是GPU id
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        meminfo = pynvml.nvmlDeviceGetMemoryInfo(handle)
        # print(meminfo.used)
        gpuTotalMem = meminfo.total
        gpuUsedMem = meminfo.used

        content = "GPU Memory:  {}% {}/{}".format(
            gpuUsedMem/gpuTotalMem,
            str(gpuUsedMem)+"M",
            str(gpuTotalMem)+"M"
            )
        return content
    except:
        err = "getGPUInfo error..."
        return err

#Get CPU info;
def getCPUInfo(interval=1):
    try:
        cpuUsage = psutil.cpu_percent(interval)
        cpuCores = psutil.cpu_count()
        content = "CPU {}% ({} cores)".format(cpuUsage,cpuCores)

        return content
    except:
        err = "getCPUInfo error..."
        return err

#Get memory info
def getMemoryInfo(): 
    try:
        phyMem = psutil.virtual_memory()
        percentMem = phyMem.percent 
        usedMem = int(phyMem.used/1024/1024)
        totalMem = int(phyMem.total/1024/1024)

        content = "Memory: {}% {}/{}".format(
            percentMem,
            str(usedMem)+"M",
            str(totalMem)+"M"
            )

        return content
    except:
        err = "getMemoryInfo error..."
        return err

#Get disk info
def getDiskInfo():
    try:
        disk_partitions = {}
        partitions = psutil.disk_partitions()
        for i,p in enumerate(partitions):
            usage = psutil.disk_usage(p.mountpoint)
            disk_partitions[i] = {
                'path': p.mountpoint,
                'total': usage.total,
                'used': usage.used,
                'percent': usage.percent
            } 

        return disk_partitions
    except:
        err = "getDiskInfo error..."
        return err

#Get user info
def getUserInfo():
    try:
        users = psutil.users()
        usersInfo = {}

        for i,u in enumerate(users):
            struct_time = time.localtime(u.started) # 轉成時間元組
            # print(struct_time.tm_year)
            timeString = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
            usersInfo[i] = {'name':u.name,'startTime':timeString}

        return usersInfo
    except:
        err = "getUserInfo error..."
        return err


# if __name__ == "__main__":
#     while True:
#         print(getCPUInfo())
#         print(getGPUInfo())
#         print(getMemoryInfo())
#         print(getDiskInfo())
#         print(getUserInfo())
#         time.sleep(5)
    