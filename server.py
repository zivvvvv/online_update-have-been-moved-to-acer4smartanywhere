# import time
# import http.server
# import json
# HOST_NAME = 'localhost' 
# PORT_NUMBER = 8000 
# class MyHandler(http.server.BaseHTTPRequestHandler):
#     def do_POST(self):
#         print("You've got mail!")
#         content_len = int(self.headers.get('content-length', 0))
#         body = self.rfile.read(content_len)
#         data = json.loads(body)
#         print(data)
#         print(self.send_response(0,message=None))
        
# if __name__ == '__main__':
#     server_class = http.server.HTTPServer
#     httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
#     print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, 
# PORT_NUMBER))
#     try:
#         httpd.serve_forever()
#     except KeyboardInterrupt:
#         pass
#     httpd.server_close()
#     print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))


from flask import Flask, jsonify, request
import json
app = Flask(__name__)

@app.route('/SendInfo', methods=['POST'])
def SendInfo():
    input = request.get_data()
    data = json.loads(input)
    print(data)

    # status, description
    return jsonify({'status':True,"description":"description"})
    
    
if __name__ == "__main__":
    app.run(port=9000, debug=True)