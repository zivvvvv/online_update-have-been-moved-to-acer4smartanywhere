import sys
import requests
import json
import time
import getInfo
import threading

# def run():
#     url = 'http://localhost:5001'
#     # data = {
#     #     'date': 'the best day',
#     #     'time': 'the best time',
#     #     'name': 'Janet Smith'
#     # }
    
#     # sleepTime = 5*60
#     sleepTime = 5
    

#     while True:

#         data = {
#             'cpu':getInfo.getCPUInfo(),
#             'gpu':getInfo.getGPUInfo(),
#             'memory':getInfo.getMemoryInfo(),
#             'disk':getInfo.getDiskInfo(),
#             'user':getInfo.getUserInfo()
#         }

#         # print("data: {}".format(sys.getsizeof(data)))

#         r = requests.post(url,json=data)
#         # print(r.status_code)
#         # print(r.json())
#         time.sleep(sleepTime)



def run(number):
    url = 'http://localhost:5001'
    
    # data = {
    #     'date': 'the best day',
    #     'time': 'the best time',
    #     'name': 'Janet Smith'
    # }
    
    # sleepTime = 5*60
    sleepTime = 0.01
    

    while True:

        struct_time = time.localtime() # 轉成時間元組
        # print(struct_time.tm_min)
        if struct_time.tm_min != 2:
            time.sleep(sleepTime)
            continue

        data = {
            'number':number,
            'cpu':getInfo.getCPUInfo(),
            'gpu':getInfo.getGPUInfo(),
            'memory':getInfo.getMemoryInfo(),
            'disk':getInfo.getDiskInfo(),
            'user':getInfo.getUserInfo()
        }

        # print("data: {}".format(sys.getsizeof(data)))

        r = requests.post(url,json=data)
        # print(r.status_code)
        # print(r.json())
        time.sleep(sleepTime)




if __name__ == '__main__':
    # run()

    thread = list()

    # for i in range(1000):
    #     thread.append(threading.Thread(target = run, args=(i,)))

    # for i in range(1000):
    #     try:
    #         print('{}'.format(i))
    #         time.sleep(0.1)
    #         thread[i].start()
    #     except:
    #         print("error {}".format(i))
    
    
    # print(timeString)
    
    for i in range(100):
        try:
            print('{}'.format(i))
            # time.sleep(0.001)
            t1 = threading.Thread(target = run, args=(i,))
            t1.start()
        except:
            print("error {}".format(i))
