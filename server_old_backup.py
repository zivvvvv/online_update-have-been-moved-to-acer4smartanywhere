# import time
# import http.server
# import json
# HOST_NAME = 'localhost' 
# PORT_NUMBER = 8000 
# class MyHandler(http.server.BaseHTTPRequestHandler):
#     def do_POST(self):
#         print("You've got mail!")
#         content_len = int(self.headers.get('content-length', 0))
#         body = self.rfile.read(content_len)
#         data = json.loads(body)
#         print(data)
#         print(self.send_response(0,message=None))
        
# if __name__ == '__main__':
#     server_class = http.server.HTTPServer
#     httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
#     print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, 
# PORT_NUMBER))
#     try:
#         httpd.serve_forever()
#     except KeyboardInterrupt:
#         pass
#     httpd.server_close()
#     print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))


from http.server import BaseHTTPRequestHandler, HTTPServer
import socketserver
import json
import cgi

class Server(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        
    def do_HEAD(self):
        self._set_headers()
        
    # GET sends back a Hello world message
    def do_GET(self):
        self._set_headers()
        self.wfile.write(json.dumps({'hello': 'world', 'received': 'ok'}))
        
    # POST echoes the message adding a JSON field
    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers['content-type'])
        
        # refuse to receive non-json content
        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return
            
        # read the message and convert it into a python dictionary
        length = int(self.headers.get('content-length'))
        message = json.loads(self.rfile.read(length))
        
        # add a property to the object, just to mess with data
        message['received'] = 'ok'
        # send the message back
        self._set_headers()
        self.wfile.write(json.dumps(message).encode())

        print('{}\n'.format(message))
        
def run(server_class=HTTPServer, handler_class=Server, port=5001):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    
    print('Starting httpd on port %d...' % port)
    httpd.serve_forever()
    
if __name__ == "__main__":
    from sys import argv
    
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()